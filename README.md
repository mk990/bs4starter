# Bootstrap 4 Starter Pack 

Includes complete Bootstrap 4.0.0 dev environment with gulp and sass and es6

### Version

1.0.0

## Install Dependencies

```bash
npm i
```

## Compile Sass & Run Dev Server

```bash
npm start
```

Files are compiled into /dist